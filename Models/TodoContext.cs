using Microsoft.EntityFrameworkCore;  
namespace Task8.Models
{
public class TodoContext : DbContext     
  {         
    public TodoContext(DbContextOptions<TodoContext> options)                            : base(options)         
{         
}       
    public DbSet<TodoItem> TodoItems { get; set; }     
    
   }
   
    }