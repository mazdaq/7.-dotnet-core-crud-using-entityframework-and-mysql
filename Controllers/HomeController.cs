﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Task8.Models;

namespace Task8.Controllers
{
    public class HomeController : Controller
    {
     
        //Employee Context
         private readonly EmployeeContext _empContext;

        //Construcotr
         public HomeController(EmployeeContext context)
         {
            //Initializing context
             _empContext = context;
         }
        
        //Dispaly Employee List
        public IActionResult Index()
        {

        //Get employee list and copy list to a variable "list"
        var list = _empContext.EmployeeRecord.ToList();
                      
            //pass list to view
            return View(list);
        }

    //adding new employee - (view)
    public IActionResult New()
    {
        //creating model of employee and passing it to view
            employeerecord model = new employeerecord();
        return View(model);
    }

    [HttpPost]//getting employee info from View and saving it
    public IActionResult New(employeerecord model)
    {
        //Adding received employee information into databse record
        _empContext.EmployeeRecord.Add(new employeerecord{
            firstname = model.firstname,
            lastname = model.lastname,
            job = model.job,
            salary = model.salary
        });
        _empContext.SaveChanges(); //saving changes 
        return RedirectToAction("Index");
    }
    
    //update new employees - (View)    
    public IActionResult Update()
    {
        //creating employee model and passing it to view
        employeerecord model = new employeerecord();
        return View(model);
    }

    [HttpPost]//updating employee information
    public IActionResult Update(employeerecord model)
    {

        //updating employee record based on id provided 
        employeerecord m = _empContext.EmployeeRecord.FirstOrDefault(e=>e.id.Equals(model.id));
        m.firstname = model.firstname;
        m.lastname = model.lastname;
        m.job = model.job;
        m.salary =  model.salary ;
        _empContext.SaveChanges(); //saving changes

    return RedirectToAction("Index");

    }

    //deleting employee record
    public IActionResult Delete(int id)
    {
        //deleting employee record based on id provided
        //using Linq Query
        var query =(from p in _empContext.EmployeeRecord
        where p.id.Equals(id)
        select p).FirstOrDefault();
        
        //Remove employee record
        _empContext.EmployeeRecord.Remove(query);

        //saving changes
        _empContext.SaveChanges();
        return RedirectToAction("Index");
    }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
