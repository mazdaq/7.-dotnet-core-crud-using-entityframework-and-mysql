-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 19, 2019 at 09:51 PM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `employees`
--

-- --------------------------------------------------------

--
-- Table structure for table `employeerecord`
--

DROP TABLE IF EXISTS `employeerecord`;
CREATE TABLE IF NOT EXISTS `employeerecord` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` longtext,
  `lastname` longtext,
  `job` longtext,
  `salary` longtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employeerecord`
--

INSERT INTO `employeerecord` (`id`, `firstname`, `lastname`, `job`, `salary`) VALUES
(1, 'Mazdaq', 'Shahzad', 'Software Eningeer', '25000'),
(5, 'Faraz', 'Abbas', 'Mechnacia lEnginer', '2342394239992394239492394234'),
(4, 'Asad', 'Abbas', 'Softwae Enigneer', '24324');

-- --------------------------------------------------------

--
-- Table structure for table `todoitems`
--

DROP TABLE IF EXISTS `todoitems`;
CREATE TABLE IF NOT EXISTS `todoitems` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Name` longtext,
  `IsComplete` bit(1) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `todoitems`
--

INSERT INTO `todoitems` (`Id`, `Name`, `IsComplete`) VALUES
(1, 'mazdaq', b'1'),
(2, 'Mazdaq', b'1'),
(3, 'Mazdaq', b'1'),
(4, 'Item1', b'0'),
(5, 'Item1', b'0'),
(6, 'Item1', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `__efmigrationshistory`
--

DROP TABLE IF EXISTS `__efmigrationshistory`;
CREATE TABLE IF NOT EXISTS `__efmigrationshistory` (
  `MigrationId` varchar(95) NOT NULL,
  `ProductVersion` varchar(32) NOT NULL,
  PRIMARY KEY (`MigrationId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `__efmigrationshistory`
--

INSERT INTO `__efmigrationshistory` (`MigrationId`, `ProductVersion`) VALUES
('20190218060438_initial', '2.1.4-rtm-31024'),
('20190218062733_employe', '2.1.4-rtm-31024');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
